# Ansible Intro - Lab

## Prerequisites: Vagrant and VirtualBox

### Install VirtualBox

https://www.virtualbox.org/wiki/Downloads

### Vagrant

#### Install Vagrant

https://www.vagrantup.com/docs/installation


- Fedora 32
```sh
sudo dnf install vagrant
```

#### Test Vagrant

Check version

```sh
vagrant --version
```

Create a test vm
```sh
mkdir vagrant-test
cd vagrant-test
vagrant init hashicorp/bionic64
ls
vagrant up
vagrant status
vagrant box list
```

```sh
vagrant ssh
exit
```


Clean up
```sh
vagrant halt
vagrant destroy -f
cd ..
rm -fr vagrant-test
vagrant box remove hashicorp/bionic64
vagrant box list
```


## Setting up VMs

### Prepare home directory
```sh
mkdir vms
cd vms
```

### Create Web Server X (replace X for 1 and then 2)

```sh
mkdir webserver-X
cd webserver-X
vagrant init centos/8
```

```sh
vi Vagrantfile
  config.vm.network "private_network", type: "dhcp"
```

Generate ssh keys
```sh
ssh-keygen
ls ~/.ssh/
```

```sh
vagrant up

vagrant ssh

vagrant ssh -c 'ip addr show eth1'

ssh vagrant@<IPADDRESS_X>

ssh -i ./.vagrant/machines/default/virtualbox/private_key vagrant@<IPADDRESS_X>

ssh-copy-id -f -o 'IdentityFile ./.vagrant/machines/default/virtualbox/private_key' vagrant@<IPADDRESS_X>

ssh vagrant@<IPADDRESS_X>
```

In a new terminal:
```sh
ping <IPADDRESS_X>
```

In a new terminal:
```sh
watch -d 'ssh vagrant@<IPADDRESS_X> uname -r'
```

In a new terminal:
```sh
watch -d curl -Ss <IPADDRESS_X>
```

## Run playbook

### Update inventory

```sh
ansible webservers -i hosts --list-hosts
```

### Test Ansible connection (ping)

```sh
ansible webservers -i hosts -u vagrant -m ping
```

### Run playbook
```sh
ansible-playbook -i hosts deploy-website.yml
```

Change `index.html`

```sh
ansible-playbook -i hosts deploy-website.yml
```

### Clean up (replace X for 1 and then 2)

```sh
cd vms
cd webserver-X
vagrant halt
vagrant destroy -f
cd ../..
rm -fr vms
```